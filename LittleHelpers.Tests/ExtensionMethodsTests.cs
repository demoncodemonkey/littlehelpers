﻿using System;
using System.Linq;
using System.Collections.Generic;
using NUnit.Framework;

namespace demoncodemonkey.LittleHelpers
{
	public class ExtensionMethodsTests
	{
		[TestFixture]
		public class Objects
		{
			[SetUp]
			public void Setup()
			{
			}

			#region IsNull
			[Test]
			public void IsNull_ShouldReturnTrue_WithNullObject()
			{
				object input = null;
				Assert.IsTrue(input.IsNull());
			}

			[Test]
			public void IsNull_ShouldReturnTrue_WithNullString()
			{
				string input = null;
				Assert.IsTrue(input.IsNull());
			}

			[Test]
			public void IsNull_ShouldReturnFalse_WithEmptyString()
			{
				string input = string.Empty;
				Assert.IsFalse(input.IsNull());
			}

			[Test]
			public void IsNull_ShouldReturnFalse_WithNewStringArray()
			{
				var input = new string[] { "aaa", "bbb" };
				Assert.IsFalse(input.IsNull());
			}
			#endregion

			#region IsNotNull
			[Test]
			public void IsNotNull_ShouldReturnFalse_WithNullObject()
			{
				object input = null;
				Assert.IsFalse(input.IsNotNull());
			}

			[Test]
			public void IsNotNull_ShouldReturnFalse_WithNullString()
			{
				string input = null;
				Assert.IsFalse(input.IsNotNull());
			}

			[Test]
			public void IsNotNull_ShouldReturnTrue_WithEmptyString()
			{
				string input = string.Empty;
				Assert.IsTrue(input.IsNotNull());
			}

			[Test]
			public void IsNotNull_ShouldReturnTrue_WithNewStringArray()
			{
				var input = new string[] { "aaa", "bbb" };
				Assert.IsTrue(input.IsNotNull());
			}
			#endregion

			#region IsIn
			//System.Random used for testing, as it is an easily instantiatable class
			[Test]
			[ExpectedException(typeof(ArgumentNullException))]
			public void IsIn_ShouldThrow_WithNullSource()
			{
				Random input = null;
				var target = new Random();
				var testItems = new[] { target };
                bool result = input.IsIn(testItems);
			}

			[Test]
			[ExpectedException(typeof(ArgumentNullException))]
			public void IsIn_ShouldThrow_WithNullArgument()
			{
				var input = new Random();
                Random[] testItems = null;
                bool result = input.IsIn(testItems);
			}

			[Test]
			public void IsIn_ShouldReturnFalse_WithEmptyArgument()
			{
				var input = new Random();
                var testItems = new Random[0];
                Assert.IsFalse(input.IsIn(testItems));
			}

			[Test]
			public void IsIn_ShouldReturnFalse_WhenLookingForNonMatchingObject()
			{
				var input = new Random();
				var target = new Random();
                var testItems = new[] { target };
                Assert.IsFalse(input.IsIn(testItems));
			}

			[Test]
			public void IsIn_ShouldReturnTrue_WhenLookingForIdenticalObject()
			{
				var input = new Random();
				var target = new Random();
                var testItems = new[] { target, input };
                Assert.IsTrue(input.IsIn(testItems));
			}

			#endregion

			#region IsNotIn
			//System.Random used for testing, as it is an easily instantiatable class
			[Test]
			[ExpectedException(typeof(ArgumentNullException))]
			public void IsNotIn_ShouldThrow_WithNullSource()
			{
				Random input = null;
				var target = new Random();
                var testItems = new[] { target };
                bool result = input.IsNotIn(testItems);
			}

			[Test]
			[ExpectedException(typeof(ArgumentNullException))]
			public void IsNotIn_ShouldThrow_WithNullArgument()
			{
				var input = new Random();
                Random[] testItems = null;
                bool result = input.IsNotIn(testItems);
			}

			[Test]
			public void IsNotIn_ShouldReturnTrue_WithEmptyArgument()
			{
				var input = new Random();
                var testItems = new Random[0];
                Assert.IsTrue(input.IsNotIn(testItems));
			}

			[Test]
			public void IsNotIn_ShouldReturnTrue_WhenLookingForNonMatchingObject()
			{
				var input = new Random();
				var target = new Random();
                var testItems = new[] { target };
                Assert.IsTrue(input.IsNotIn(testItems));
			}

			[Test]
			public void IsNotIn_ShouldReturnFalse_WhenLookingForIdenticalObject()
			{
				var input = new Random();
				var target = new Random();
                var testItems = new[] { target, input };
                Assert.IsFalse(input.IsNotIn(testItems));
			}

			#endregion
		}

        [TestFixture]
		public class Strings
		{
            [SetUp]
			public void Setup()
			{
			}

			#region IsEmpty
			[Test]
			[ExpectedException(typeof(ArgumentNullException))]
			public void IsEmpty_ShouldThrow_WithNullString()
			{
				string input = null;
				bool result = input.IsEmpty();
			}

			[Test]
			public void IsEmpty_ShouldReturnTrue_WithEmptyString()
			{
				string input = string.Empty;
				Assert.IsTrue(input.IsEmpty());
			}

			public void IsEmpty_ShouldReturnFalse_WithNormalString()
			{
				string input = "abcdef";
				Assert.IsFalse(input.IsEmpty());
			}
			#endregion

			#region IsNotEmpty
			[Test]
			[ExpectedException(typeof(ArgumentNullException))]
			public void IsNotEmpty_ShouldThrow_WithNullString()
			{
				string input = null;
				bool result = input.IsNotEmpty();
			}

			[Test]
			public void IsNotEmpty_ShouldReturnFalse_WithEmptyString()
			{
				string input = string.Empty;
				Assert.IsFalse(input.IsNotEmpty());
			}

			public void IsNotEmpty_ShouldReturnTrue_WithNormalString()
			{
				string input = "abcdef";
				Assert.IsTrue(input.IsNotEmpty());
			}
			#endregion

			#region IsNullOrEmpty
			[Test]
			public void IsNullOrEmpty_ShouldReturnTrue_WithNullString()
			{
				string input = null;
				Assert.IsTrue(input.IsNullOrEmpty());
			}

			[Test]
			public void IsNullOrEmpty_ShouldReturnTrue_WithEmptyString()
			{
				string input = string.Empty;
				Assert.IsTrue(input.IsNullOrEmpty());
			}

			public void IsNullOrEmpty_ShouldReturnFalse_WithNormalString()
			{
				string input = "abcdef";
				Assert.IsFalse(input.IsNullOrEmpty());
			}
			#endregion

			#region IsNotNullAndNotEmpty
			[Test]
			public void IsNotNullAndNotEmpty_ShouldReturnFalse_WithNullString()
			{
				string input = null;
				Assert.IsFalse(input.IsNotNullAndNotEmpty());
			}

			[Test]
			public void IsNotNullAndNotEmpty_ShouldReturnFalse_WithEmptyString()
			{
				string input = string.Empty;
				Assert.IsFalse(input.IsNotNullAndNotEmpty());
			}

			public void IsNotNullAndNotEmpty_ShouldReturnTrue_WithNormalString()
			{
				string input = "abcdef";
				Assert.IsTrue(input.IsNotNullAndNotEmpty());
			}
			#endregion

			#region ContainsIgnoreCase
			[Test]
			public void ContainsIgnoreCase_ShouldReturnTrue_WhenLookingForIdenticalMatch()
			{
				string input = "abcdefg";
				string testMatch = "abcdefg";
				Assert.IsTrue(input.ContainsIgnoreCase(testMatch));
			}

			[Test]
			public void ContainsIgnoreCase_ShouldReturnTrue_WhenLookingForIdenticalCasedSubstring()
			{
				string input = "abcdefg";
				string testMatch = "def";
				Assert.IsTrue(input.ContainsIgnoreCase(testMatch));
			}

			[Test]
			public void ContainsIgnoreCase_ShouldReturnTrue_WhenLookingForWrongCasedSubstring()
			{
				string input = "abcdefg";
				string testMatch = "DeF";
				Assert.IsTrue(input.ContainsIgnoreCase(testMatch));
			}

			[Test]
			public void ContainsIgnoreCase_ShouldReturnFalse_WhenLookingForEmptyString()
			{
				string input = "abcdefg";
				string testMatch = "";
				Assert.IsFalse(input.ContainsIgnoreCase(testMatch));
			}

			[Test]
			public void ContainsIgnoreCase_ShouldReturnFalse_WhenLookingForNonMatchingSubstring()
			{
				string input = "abcdefg";
				string testMatch = "xxyyzz";
				Assert.IsFalse(input.ContainsIgnoreCase(testMatch));
			}

			[Test]
			public void ContainsIgnoreCase_ShouldReturnFalse_WhenLookingForStringWithinSubstring()
			{
				string input = "def";
				string testMatch = "abcdefg";
				Assert.IsFalse(input.ContainsIgnoreCase(testMatch));
			}

			[Test]
			[ExpectedException(typeof(ArgumentNullException))]
			public void ContainsIgnoreCase_ShouldThrow_WhenLookingForNull()
			{
				string input = "abcdefg";
				string testMatch = null;
				bool result = input.ContainsIgnoreCase(testMatch);
			}
			#endregion

			#region EqualsIgnoreCase
			[Test]
			[ExpectedException(typeof(ArgumentNullException))]
			public void EqualsIgnoreCase_ShouldThrow_WithNullString()
			{
				string input = null;
				string testMatch = "asd";
				bool result = input.EqualsIgnoreCase(testMatch);
			}

			[Test]
			[ExpectedException(typeof(ArgumentNullException))]
			public void EqualsIgnoreCase_ShouldThrow_WithNullStringArgument()
			{
				string input = "asd";
				string testMatch = null;
				bool result = input.EqualsIgnoreCase(testMatch);
			}

			[Test]
			public void EqualsIgnoreCase_ShouldReturnFalse_WithEmptyStringAndValidArgument()
			{
				string input = string.Empty;
				string testMatch = "asd";
				Assert.IsFalse(input.EqualsIgnoreCase(testMatch));
			}

			[Test]
			public void EqualsIgnoreCase_ShouldReturnFalse_WithValidStringAndEmptyArgument()
			{
				string input = "asd";
				string testMatch = string.Empty;
				Assert.IsFalse(input.EqualsIgnoreCase(testMatch));
			}

			[Test]
			public void EqualsIgnoreCase_ShouldReturnTrue_WithEmptyStringAndEmptyArgument()
			{
				string input = string.Empty;
				string testMatch = string.Empty;
				Assert.IsTrue(input.EqualsIgnoreCase(testMatch));
			}

			[Test]
			public void EqualsIgnoreCase_ShouldReturnTrue_WithIdenticalStrings()
			{
				string input = "abcdefg";
				string testMatch = "abcdefg";
				Assert.IsTrue(input.EqualsIgnoreCase(testMatch));
			}

			[Test]
			public void EqualsIgnoreCase_ShouldReturnTrue_WithWrongCaseStrings()
			{
				string input = "aBcdeFg";
				string testMatch = "abCDEfg";
				Assert.IsTrue(input.EqualsIgnoreCase(testMatch));
			}

			[Test]
			public void EqualsIgnoreCase_ShouldReturnFalse_WithNonMatchingStrings()
			{
				string input = "abcdefg";
				string testMatch = "isdbfjsasdasf";
				Assert.IsFalse(input.EqualsIgnoreCase(testMatch));
			}

			[Test]
			public void EqualsIgnoreCase_ShouldReturnFalse_WithNonMatchingSubstrings()
			{
				string input = "abcdefg";
				string testMatch = "isdbfjabcdefgsasdasf";
				Assert.IsFalse(input.EqualsIgnoreCase(testMatch));
			}

			#endregion

			#region IsIn (list)
			[Test]
			[ExpectedException(typeof(ArgumentNullException))]
			public void IsIn_StringList_ShouldThrow_WithNullString()
			{
				string input = null;
                var testItems = new[] { "abcdefg" };
                bool result = input.IsIn(testItems);
			}

			[Test]
			[ExpectedException(typeof(ArgumentNullException))]
			public void IsIn_StringList_ShouldThrow_WithNullArgument()
			{
				string input = "abcdefg";
                string[] testItems = null;
                bool result = input.IsIn(testItems);
			}

			[Test]
			public void IsIn_StringList_ShouldReturnFalse_WithEmptyArgument()
			{
				string input = "abcdefg";
                var testItems = new string[0];
                Assert.IsFalse(input.IsIn(testItems));
			}

			[Test]
			public void IsIn_StringList_ShouldReturnTrue_WithEmptyStringWhenListContainsEmptyString()
			{
				string input = string.Empty;
                var testItems = new[] { "abcdefg", "", "hhuuuiii" };
                Assert.IsTrue(input.IsIn(testItems));
			}

			[Test]
			public void IsIn_StringList_ShouldReturnFalse_WithEmptyStringWhenListDoesNotContainEmptyString()
			{
				string input = string.Empty;
                var testItems = new[] { "abcdefg", "xyz", "qwqweqwe" };
                Assert.IsFalse(input.IsIn(testItems));
			}

			[Test]
			public void IsIn_StringList_ShouldReturnTrue_WhenLookingForIdenticalMatchInSingleItemList()
			{
				string input = "abcdefg";
                var testItems = new[] { "abcdefg" };
                Assert.IsTrue(input.IsIn(testItems));
			}

			[Test]
			public void IsIn_StringList_ShouldReturnTrue_WhenLookingForIdenticalMatchAtStartOfMultiItemList()
			{
				string input = "abcdefg";
                var testItems = new[] { "abcdefg", "hahaha", "blahblah" };
                Assert.IsTrue(input.IsIn(testItems));
			}

			[Test]
			public void IsIn_StringList_ShouldReturnTrue_WhenLookingForIdenticalMatchAtMiddleOfMultiItemList()
			{
				string input = "abcdefg";
                var testItems = new[] { "floofloo", "abcdefg", "sosososos" };
                Assert.IsTrue(input.IsIn(testItems));
			}

			[Test]
			public void IsIn_StringList_ShouldReturnTrue_WhenLookingForIdenticalMatchAtEndOfMultiItemList()
			{
				string input = "abcdefg";
                var testItems = new[] { "kakakaoskasdasd", "halominosphart", "abcdefg" };
                Assert.IsTrue(input.IsIn(testItems));
			}

			[Test]
			public void IsIn_StringList_ShouldReturnFalse_WhenLookingForNonMatchingStringInSingleItemList()
			{
				string input = "abcdefg";
                var testItems = new[] { "nsdofbgifvcgx" };
                Assert.IsFalse(input.IsIn(testItems));
			}

			[Test]
			public void IsIn_StringList_ShouldReturnFalse_WhenLookingForNonMatchingStringInMultiItemList()
			{
				string input = "abcdefg";
                var testItems = new[] { "dfndfpogdfb", "oinsdofinsdgf", "ffttdfgineiedv" };
                Assert.IsFalse(input.IsIn(testItems));
			}

			[Test]
			public void IsIn_StringList_ShouldReturnFalse_WhenLookingForNonMatchingStringInSubstringInSingleItemList()
			{
				string input = "abcdefg";
                var testItems = new[] { "xmfgiabcdefgfdgmnd" };
                Assert.IsFalse(input.IsIn(testItems));
			}

			[Test]
			public void IsIn_StringList_ShouldReturnFalse_WhenLookingForNonMatchingStringInSubstringInMultiItemList()
			{
				string input = "abcdefg";
                var testItems = new[] { "osdfunhimfgoidtg", "sdfbabcdefgsdvsd", "vdfhfghgsjjj" };
                Assert.IsFalse(input.IsIn(testItems));
			}

			[Test]
			public void IsIn_StringList_ShouldReturnFalse_WhenLookingForWrongCaseMatchingStringInSingleItemList()
			{
				string input = "abcdefg";
                var testItems = new[] { "ABCDEFG" };
                Assert.IsFalse(input.IsIn(testItems));
			}

			[Test]
			public void IsIn_StringList_ShouldReturnFalse_WhenLookingForWrongCaseMatchingStringInMultiItemList()
			{
				string input = "abcdefg";
                var testItems = new[] { "dfndfpogdfb", "ABCDEFG", "ffttdfgineiedv" };
                Assert.IsFalse(input.IsIn(testItems));
			}

			#endregion

			#region IsIn (array)
			[Test]
			[ExpectedException(typeof(ArgumentNullException))]
			public void IsIn_StringArray_ShouldThrow_WithNullString()
			{
				string input = null;
                var testItems = new string[] { "abcdefg", };
                bool result = input.IsIn(testItems);
			}

			[Test]
			[ExpectedException(typeof(ArgumentNullException))]
			public void IsIn_StringArray_ShouldThrow_WithNullArgument()
			{
				string input = "abcdefg";
                string[] testItems = null;
                bool result = input.IsIn(testItems);
			}

			[Test]
			public void IsIn_StringArray_ShouldReturnFalse_WithEmptyArgument()
			{
				string input = "abcdefg";
                var testItems = new string[] { };
                Assert.IsFalse(input.IsIn(testItems));
			}

			[Test]
			public void IsIn_StringArray_ShouldReturnTrue_WithEmptyStringWhenListContainsEmptyString()
			{
				string input = string.Empty;
                var testItems = new string[] { "abcdefg", "", "hhuuuiii" };
                Assert.IsTrue(input.IsIn(testItems));
			}

			[Test]
			public void IsIn_StringArray_ShouldReturnFalse_WithEmptyStringWhenListDoesNotContainEmptyString()
			{
				string input = string.Empty;
                var testItems = new string[] { "abcdefg", "xyz", "qwqweqwe" };
                Assert.IsFalse(input.IsIn(testItems));
			}

			[Test]
			public void IsIn_StringArray_ShouldReturnTrue_WhenLookingForIdenticalMatchInSingleItemList()
			{
				string input = "abcdefg";
                var testItems = new string[] { "abcdefg", };
                Assert.IsTrue(input.IsIn(testItems));
			}

			[Test]
			public void IsIn_StringArray_ShouldReturnTrue_WhenLookingForIdenticalMatchAtStartOfMultiItemList()
			{
				string input = "abcdefg";
                var testItems = new string[] { "abcdefg", "hahaha", "blahblah", };
                Assert.IsTrue(input.IsIn(testItems));
			}

			[Test]
			public void IsIn_StringArray_ShouldReturnTrue_WhenLookingForIdenticalMatchAtMiddleOfMultiItemList()
			{
				string input = "abcdefg";
                var testItems = new string[] { "floofloo", "abcdefg", "sosososos", };
                Assert.IsTrue(input.IsIn(testItems));
			}

			[Test]
			public void IsIn_StringArray_ShouldReturnTrue_WhenLookingForIdenticalMatchAtEndOfMultiItemList()
			{
				string input = "abcdefg";
                var testItems = new string[] { "kakakaoskasdasd", "halominosphart", "abcdefg", };
                Assert.IsTrue(input.IsIn(testItems));
			}

			[Test]
			public void IsIn_StringArray_ShouldReturnFalse_WhenLookingForNonMatchingStringInSingleItemList()
			{
				string input = "abcdefg";
                var testItems = new string[] { "nsdofbgifvcgx", };
                Assert.IsFalse(input.IsIn(testItems));
			}

			[Test]
			public void IsIn_StringArray_ShouldReturnFalse_WhenLookingForNonMatchingStringInMultiItemList()
			{
				string input = "abcdefg";
                var testItems = new string[] { "dfndfpogdfb", "oinsdofinsdgf", "ffttdfgineiedv", };
                Assert.IsFalse(input.IsIn(testItems));
			}

			[Test]
			public void IsIn_StringArray_ShouldReturnFalse_WhenLookingForNonMatchingStringInSubstringInSingleItemList()
			{
				string input = "abcdefg";
                var testItems = new string[] { "xmfgiabcdefgfdgmnd", };
                Assert.IsFalse(input.IsIn(testItems));
			}

			[Test]
			public void IsIn_StringArray_ShouldReturnFalse_WhenLookingForNonMatchingStringInSubstringInMultiItemList()
			{
				string input = "abcdefg";
                var testItems = new string[] { "osdfunhimfgoidtg", "sdfbabcdefgsdvsd", "vdfhfghgsjjj", };
                Assert.IsFalse(input.IsIn(testItems));
			}

			[Test]
			public void IsIn_StringArray_ShouldReturnFalse_WhenLookingForWrongCaseMatchingStringInSingleItemList()
			{
				string input = "abcdefg";
                var testItems = new string[] { "ABCDEFG", };
                Assert.IsFalse(input.IsIn(testItems));
			}

			[Test]
			public void IsIn_StringArray_ShouldReturnFalse_WhenLookingForWrongCaseMatchingStringInMultiItemList()
			{
				string input = "abcdefg";
                var testItems = new string[] { "dfndfpogdfb", "ABCDEFG", "ffttdfgineiedv", };
                Assert.IsFalse(input.IsIn(testItems));
			}

			#endregion

			#region IsInIgnoreCase
			[Test]
			[ExpectedException(typeof(ArgumentNullException))]
			public void IsInIgnoreCase_ShouldThrow_WithNullString()
			{
				string input = null;
                var testItems = new[] { "abcdefg" };
                bool result = input.IsInIgnoreCase(testItems);
			}

			[Test]
			[ExpectedException(typeof(ArgumentNullException))]
			public void IsInIgnoreCase_ShouldThrow_WithNullArgument()
			{
				string input = "abcdefg";
                string[] testItems = null;
                bool result = input.IsInIgnoreCase(testItems);
			}

			[Test]
			public void IsInIgnoreCase_ShouldReturnFalse_WithEmptyArgument()
			{
				string input = "abcdefg";
                var testItems = new string[0];
                Assert.IsFalse(input.IsInIgnoreCase(testItems));
			}

			[Test]
			public void IsInIgnoreCase_ShouldReturnTrue_WithEmptyStringWhenListContainsEmptyString()
			{
				string input = string.Empty;
                var testItems = new[] { "abcdefg", "", "hhuuuiii" };
                Assert.IsTrue(input.IsInIgnoreCase(testItems));
			}

			[Test]
			public void IsInIgnoreCase_ShouldReturnFalse_WithEmptyStringWhenListDoesNotContainEmptyString()
			{
				string input = string.Empty;
                var testItems = new[] { "abcdefg", "xyz", "qwqweqwe" };
                Assert.IsFalse(input.IsInIgnoreCase(testItems));
			}
			
			[Test]
			public void IsInIgnoreCase_ShouldReturnTrue_WhenLookingForIdenticalMatchInSingleItemList()
			{
				string input = "abcdefg";
                var testItems = new[] { "abcdefg" };
                Assert.IsTrue(input.IsInIgnoreCase(testItems));
			}

			[Test]
			public void IsInIgnoreCase_ShouldReturnTrue_WhenLookingForIdenticalMatchAtStartOfMultiItemList()
			{
				string input = "abcdefg";
                var testItems = new[] { "abcdefg", "hahaha", "blahblah" };
                Assert.IsTrue(input.IsInIgnoreCase(testItems));
			}

			[Test]
			public void IsInIgnoreCase_ShouldReturnTrue_WhenLookingForIdenticalMatchAtMiddleOfMultiItemList()
			{
				string input = "abcdefg";
                var testItems = new[] { "floofloo", "abcdefg", "sosososos" };
                Assert.IsTrue(input.IsInIgnoreCase(testItems));
			}

			[Test]
			public void IsInIgnoreCase_ShouldReturnTrue_WhenLookingForIdenticalMatchAtEndOfMultiItemList()
			{
				string input = "abcdefg";
                var testItems = new[] { "kakakaoskasdasd", "halominosphart", "abcdefg" };
                Assert.IsTrue(input.IsInIgnoreCase(testItems));
			}

			[Test]
			public void IsInIgnoreCase_ShouldReturnFalse_WhenLookingForNonMatchingStringInSingleItemList()
			{
				string input = "abcdefg";
                var testItems = new[] { "nsdofbgifvcgx" };
                Assert.IsFalse(input.IsInIgnoreCase(testItems));
			}

			[Test]
			public void IsInIgnoreCase_ShouldReturnFalse_WhenLookingForNonMatchingStringInMultiItemList()
			{
				string input = "abcdefg";
                var testItems = new[] { "dfndfpogdfb", "oinsdofinsdgf", "ffttdfgineiedv" };
                Assert.IsFalse(input.IsInIgnoreCase(testItems));
			}

			[Test]
			public void IsInIgnoreCase_ShouldReturnFalse_WhenLookingForNonMatchingStringInSubstringInSingleItemList()
			{
				string input = "abcdefg";
                var testItems = new[] { "xmfgiabcdefgfdgmnd" };
                Assert.IsFalse(input.IsInIgnoreCase(testItems));
			}

			[Test]
			public void IsInIgnoreCase_ShouldReturnFalse_WhenLookingForNonMatchingStringInSubstringInMultiItemList()
			{
				string input = "abcdefg";
                var testItems = new[] { "osdfunhimfgoidtg", "sdfbabcdefgsdvsd", "vdfhfghgsjjj" };
                Assert.IsFalse(input.IsInIgnoreCase(testItems));
			}

			[Test]
			public void IsInIgnoreCase_ShouldReturnTrue_WhenLookingForWrongCaseMatchingStringInSingleItemList()
			{
				string input = "abcdefg";
                var testItems = new[] { "ABcdeFG" };
                Assert.IsTrue(input.IsInIgnoreCase(testItems));
			}

			[Test]
			public void IsInIgnoreCase_ShouldReturnTrue_WhenLookingForWrongCaseMatchingStringInMultiItemList()
			{
				string input = "abcdefg";
                var testItems = new[] { "dfndfpogdfb", "ABcdeFG", "ffttdfgineiedv" };
                Assert.IsTrue(input.IsInIgnoreCase(testItems));
			}
			#endregion
		}

        [TestFixture]
		public class IEnumerables
		{
			#region IsEmpty
			[Test]
			[ExpectedException(typeof(ArgumentNullException))]
			public void IsEmpty_ShouldThrow_WithNullList()
			{
				List<string> input = null;
				bool result = input.IsEmpty();
			}

			[Test]
			public void IsEmpty_ShouldReturnTrue_WithEmptyList()
			{
				var input = new List<string>();
				Assert.IsTrue(input.IsEmpty());
			}

			public void IsEmpty_ShouldReturnFalse_WithSingleItemList()
			{
				var input = new List<string> { "abcdef" };
				Assert.IsFalse(input.IsEmpty());
			}

			public void IsEmpty_ShouldReturnFalse_WithMultiItemList()
			{
				var input = new List<string> { "abcdef", "ghijkl" };
				Assert.IsFalse(input.IsEmpty());
			}

			public void IsEmpty_ShouldReturnFalse_WithListOfEmptyString()
			{
				var input = new List<string> { "" };
				Assert.IsFalse(input.IsEmpty());
			}
			#endregion

			#region IsNotEmpty
			[Test]
			[ExpectedException(typeof(ArgumentNullException))]
			public void IsNotEmpty_ShouldThrow_WithNullList()
			{
				List<string> input = null;
				bool result = input.IsNotEmpty();
			}

			[Test]
			public void IsNotEmpty_ShouldReturnFalse_WithEmptyList()
			{
				var input = new List<string>();
				Assert.IsFalse(input.IsNotEmpty());
			}

			public void IsNotEmpty_ShouldReturnTrue_WithSingleItemList()
			{
				var input = new List<string> { "abcdef" };
				Assert.IsTrue(input.IsNotEmpty());
			}

			public void IsNotEmpty_ShouldReturnTrue_WithMultiItemList()
			{
				var input = new List<string> { "abcdef", "ghijkl" };
				Assert.IsTrue(input.IsNotEmpty());
			}

			public void IsNotEmpty_ShouldReturnTrue_WithListOfEmptyString()
			{
				var input = new List<string> { "" };
				Assert.IsTrue(input.IsNotEmpty());
			}
			#endregion

			#region GetItemsContents

			[Test]
			[ExpectedException(typeof(ArgumentNullException))]
			public void GetItemsContents_ShouldThrow_WhenGivenNull()
			{
				List<string> input = null;
				IEnumerable<string> output = input.GetItemsContents().ToList();
			}

			[Test]
			public void GetItemsContents_ShouldReturnEmptyStringList_WhenGivenEmptyList()
			{
				var input = new List<string>();
				IEnumerable<string> output = input.GetItemsContents();
				Assert.AreEqual(0, output.Count());
			}

			[Test]
			public void GetItemsContents_ShouldReturnValidStringList_WhenGivenListOfOneEmptyString()
			{
				var input = new List<string> { "" };
				IEnumerable<string> output = input.GetItemsContents();
				Assert.AreEqual(1, output.Count());
				Assert.AreEqual("", output.FirstOrDefault());
			}

			[Test]
			public void GetItemsContents_ShouldReturnValidStringList_WhenGivenListOfOneString()
			{
				var input = new List<string> { "abcdef" };
				IEnumerable<string> output = input.GetItemsContents();
				Assert.AreEqual(1, output.Count());
				Assert.AreEqual("abcdef", output.FirstOrDefault());
			}

			[Test]
			public void GetItemsContents_ShouldReturnValidStringList_WhenGivenListOfMultiStrings()
			{
				var input = new List<string> { "abc", "def", "ghi" };
				string[] output = input.GetItemsContents().ToArray();
				Assert.AreEqual(3, output.Count());
				Assert.IsTrue("abc".IsIn(output));
				Assert.IsTrue("def".IsIn(output));
				Assert.IsTrue("ghi".IsIn(output));
			}

			#endregion
		}
	}
}
