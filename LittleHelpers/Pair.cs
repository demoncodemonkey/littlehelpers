﻿using System;

namespace demoncodemonkey.LittleHelpers
{
    public class Pair<T1,T2>
    {
        public T1 Left { get; private set; }
        public T2 Right { get; private set; }

        public Pair(T1 left, T2 right)
        {
            Left = left;
            Right = right;
        }
    }
}
