﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace demoncodemonkey.LittleHelpers
{
    public static class ObjectExtensionMethods
    {
        public static bool IsNull<T>(this T @this)
        {
            return @this == null;
        }

        public static bool IsNotNull<T>(this T @this)
        {
            return !@this.IsNull();
        }

        public static bool IsIn<T>(this T @this, params T[] values)
        {
            if (@this.IsNull())
                throw new ArgumentNullException("@this is null");
            if (values.IsNull())
                throw new ArgumentNullException("list is null");
            return values.Any(x => x.Equals(@this));
        }

        public static bool IsNotIn<T>(this T @this, params T[] values)
        {
            return !@this.IsIn(values);
        }
    }
}
