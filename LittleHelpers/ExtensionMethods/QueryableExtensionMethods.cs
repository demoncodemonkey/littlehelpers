﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;

namespace demoncodemonkey.LittleHelpers
{
    public static class QueryableExtensionMethods
    {
        public static IQueryable<string[]> SelectColumns(this IQueryable @this, string[] columnNames)
        {
            var results = @this.Select(string.Format("new({0})", string.Join(",", columnNames)));
            var listData = new List<string[]>();
            foreach (var item in @this)
            {
                var sublist = new List<string>();
                foreach (var columnName in columnNames)
                {
                    var propValue = Utils.GetPropertyValue(item, columnName);
                    sublist.Add((propValue ?? string.Empty).ToString());
                }
                listData.Add(sublist.ToArray());
            }
            return listData.AsQueryable();
        }
    }
}
