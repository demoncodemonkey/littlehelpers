﻿using System;
using System.Configuration;

namespace demoncodemonkey.LittleHelpers
{
    public static class WebConfig
    {
        /// <summary>
        /// Get an app-setting from the web.config as a string. Throws an exception if the specified setting is not found.
        /// </summary>
        public static string GetOrThrow(string key)
        {
            string value = ConfigurationManager.AppSettings[key];
            if (value == null)
                throw new Exception(string.Format("Config setting '{0}' not found.", key));

            return value;
        }

        /// <summary>
        /// Get an app-setting from the web.config as a string. The defaultValue will be returned if the specified setting is not found.
        /// </summary>
        public static string Get(string key, string defaultValue = "")
        {
            return ConfigurationManager.AppSettings[key] ?? defaultValue;
        }

        /// <summary>
        /// Get an app-setting from the web.config as a bool. The defaultValue will be returned if the specified setting is not found or cannot be parsed.
        /// </summary>
        public static bool GetBool(string key, bool defaultValue = false)
        {
            string configValue = Get(key);

            bool result;
            if (bool.TryParse(configValue, out result))
                return result;

            return defaultValue;
        }

        /// <summary>
        /// Get an app-setting from the web.config as an int. The defaultValue will be returned if the specified setting is not found or cannot be parsed.
        /// </summary>
        public static int GetInt(string key, int defaultValue = 0)
        {
            string configValue = Get(key);

            int result;
            if (int.TryParse(configValue, out result))
                return result;

            return defaultValue;
        }
    }
}
