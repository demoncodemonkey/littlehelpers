﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace demoncodemonkey.LittleHelpers
{
    public static class Utils
    {
        public static void Swap<T>(ref T valueA, ref T valueB)
        {
            T temp = valueA;
            valueA = valueB;
            valueB = temp;
        }

        public static object GetPropertyValue(object obj, string propName)
        {
            PropertyInfo propInfo = obj.GetType().GetProperty(propName);
            return propInfo.GetValue(obj, null);
        }

        public static string GetEnumDisplayName<T>(this T @this) where T : struct, IConvertible // constrain to enum (as much as possible)
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException("T must be an enumerated type");
            }

            var enumType = typeof(T);
            var field = enumType.GetFields().First(x => x.Name == Enum.GetName(enumType, @this));

            var attribute = field.GetCustomAttribute<DisplayAttribute>();

            return attribute != null ? attribute.Name : @this.ToString();
        }
    }
}
